/* JS Functions */

/* Resize all product height */

function setmaxHeight(){

    $('.products .product').each(function(){
        $(this).css('height','auto');
    });

    $('.products .product-item').each(function(){
        $(this).css('height','auto');
    });

    console.log('resize auto');

    
    var maxHeight = 0;

    $('.products .product').each(function(){
        if ($(this).height() > maxHeight) { maxHeight = $(this).height(); }
    });

    $('.products .product').height(maxHeight);

    var maxHeight2 = 0;

    $('.products .product-item').each(function(){
       if ($(this).height() > maxHeight2) { maxHeight2 = $(this).height(); }
    });

    $('.products .product-item').height(maxHeight2);

}


/* Fixed on Scroll Function */


function scrollFunction() {
    if (document.body.scrollTop > 80 || document.documentElement.scrollTop > 80) {
        $("#fixedmenu").addClass("fixed");
    } else {
        $("#fixedmenu").removeClass("fixed");
    }
}


"use strict";
var sel;
(function ($) {



    /* On Ready Functions */

    console.log('This is a Sevenloft Production, made with love!')

    $(document).ready(function(){


    $('ul.dropdown-menu [data-toggle=dropdown]').on('click', function(event) {
            event.preventDefault(); 
            event.stopPropagation(); 
            $(this).parent().siblings().removeClass('open');
            $(this).parent().toggleClass('open');
        });

    setmaxHeight();


    $('.navbar-toggler').click(function () {
        $(this).toggleClass('active');
           
    });

    $('.pf-help-title').click(function(){
        $(this).next('.prdctfltr_add_scroll').slideToggle();
    });

    $('.slider .owl-carousel').owlCarousel({
        loop:false,
        items: 1,
        nav:true,
        dots:false,
        autoplay: true,
    });

    $('.center').owlCarousel({
          center: true,
          loop:true,
          items: 3,
          nav:false,
          margin: 10,
          dots:false,
          autoplay: true,
          responsive : {
            0 : {
                items : 1
            },
            480 : {
                items : 1
            },
            768 : {
                items : 1
            },
            992 : {
                items : 1
            },
            1024: {
              items: 2
            }
        }
    });


  	$('.selected-products-slider.owl-carousel').owlCarousel({
          loop:true,
          items: 1,
          nav:false,
          dots:true
  	});

    $('.logos-slider .owl-carousel').owlCarousel({
        loop:true,
        items: 5,
        nav:true,
        dots:false,
        margin:15,
        navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
        responsive : {
            0 : {
                items : 2
            },
            480 : {
                items : 3
            },
            768 : {
                items : 4
            },
            992 : {
                items : 5
            }
        }
    });

    $('#tabs .woocommerce.columns-4').removeClass("woocommerce columns-4");
    $('#tabs .products li').wrap('<div class="tab-wr"></div>');

    $('#tab_new ul').addClass('owl-carousel');
    $('#tab_new ul .tab-wr').addClass('item');

    $('#tab_new ul.owl-carousel').owlCarousel({
      loop:true,
      items: 4,
      nav:true,
      dots:false,
      navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
      responsive : {
          0 : {
              items : 1
          },
          480 : {
              items : 1
          },
          768 : {
              items : 3
          },
          992 : {
              items : 4
          }
      }
    });

      $('.center2').owlCarousel({
          loop:true,
          items: 4,
          nav:true,
          dots:false,
          autoplay: true,
           navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
    });



    $('#menuCloseBtn').click(function(){
        $('section.row-offcanvas.row-offcanvas-left').removeClass('active');
    });


    $('.single-product .woocommerce-message a.button.wc-forward').attr('href','javascript:void(0)');
    $('.single-product .woocommerce-message a.button.wc-forward').attr('onclick', 'openNav()');

    $('table').addClass('table');

    function changeAvailabilityString(){
        if($('body').hasClass('single-product')){

            if ( $('.woocommerce-variation-custom-availability').length > 0  ){
                $('.custom_availability').text($('.woocommerce-variation-custom-availability').text());
                var cstAvailabitity = $('.woocommerce-variation-custom-availability').text();

                if (cstAvailabitity == 'available'){
                    $('.woocommerce-variation-custom-availability').text('Άμεση παραλαβή / Παράδoση 1 έως 3 ημέρες');
                } else if (cstAvailabitity == '4-10-days') {
                    $('.woocommerce-variation-custom-availability').text('Παράδοση σε 4 - 10 ημέρες');
                } else if (cstAvailabitity == 'after-order') {
                    $('.woocommerce-variation-custom-availability').text('Κατόπιν Παραγγελίας');
                } else if (cstAvailabitity == 'sold-out') {
                    $('.woocommerce-variation-custom-availability').text('Εξαντλημένο');
                }
            }
        }       
    }


    setInterval( changeAvailabilityString , 50);


    $('nav ul ul, nav ul ul li').css("visibility","visible");

    
    $('.col-md-12.col-sm-12.col-12 .related > h2').text('Συνδίασε το με..');
    $('.col-12 .related > h2').text('Σχετικά προϊοντα');


    $('.sale-product-wr .products li').each(function(){
      $(this).addClass('prod-item');
    });


    var wdivide = $(window).width() / 2;


    $('.featured-products-wr .products').css('height', 'auto');

    $('.featured-products-wr .products').css('visibility', 'visible');
    function changeTextCheckout(){
        $('.woocommerce-terms-and-conditions-checkbox-text a').text('όρους και προυποθέσεις');    
    }
  
    if ($('body').hasClass('woocommerce-checkout')){
        setTimeout(changeTextCheckout, 2000);
    }
 


          
    function genericSocialSharefb(){

        window.open('https://www.facebook.com/sharer.php?u='+window.location.href,'sharer','toolbar=0,status=0,width=648,height=395');
        return true;
    };

    function genericSocialSharegp(){

        window.open('https://plus.google.com/share?url='+window.location.href,'sharer','toolbar=0,status=0,width=648,height=395');
        return true;
    };

    function genericSocialSharetw(){

        window.open('http://twitter.com/intent/tweet?url='+window.location.href,'sharer','toolbar=0,status=0,width=648,height=395');
        return true;
    };

    function genericSocialSharePint(){
        window.open('http://pinterest.com/pin/create/button/?url='+window.location.href+'&media='+document.getElementsByTagName("img")[1].src,'sharer','toolbar=0,status=0,width=648,height=395');
        return true;
    };

    $(".myBox").click(function () {
        window.location = $(this).find("a").attr("href");
        return false;
    });

    

    $('.brand-carousel').owlCarousel({
      loop:true,
      margin:10,
      autoplay:true,
      responsive:{
        0:{
          items:1
        },
        600:{
          items:3
        },
        1000:{
          items:5
        }
      }
    })


    $('.products-title-drop').on('click', function (e) {
      e.preventDefault();
      var target = $(this).find('.product-content');

      if (!target.hasClass('show')) {
          target.addClass('show');
          target.parents('.products-title-drop').addClass('active');
      } else {
          target.removeClass('show');
          target.parents('.products-title-drop').removeClass('active');
      }
    });

}); 

/* On Load Functions */

$(window).load(function() {

  setmaxHeight();

});



/* On Scroll Functions */

$(window).scroll(function(){
        if ($(window).width() > 991){
            var sticky = $('.fixed-menu'),
                scroll = $(window).scrollTop();

            if (scroll >= 260) {
                sticky.addClass('fixed');
                sticky.slideDown();
            }
            else {
                sticky.removeClass('fixed');
                sticky.slideUp();
            }       
        }


      if ($(window).scrollTop() >700) {
        btn.addClass('show');
      } else {
        btn.removeClass('show');
      }

      scrollFunction();

});


var btn = $('#button');
    
btn.on('click', function(e) {
  e.preventDefault();
  $('html, body').animate({scrollTop:0}, '700');
});


/* On Resize Functions */


$( window ).resize(function() {
    setmaxHeight();
});




})(jQuery);









