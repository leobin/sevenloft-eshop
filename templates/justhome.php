<?php
/*Template Name: Home */
?>

<?php get_header(); ?>


<?php do_action('sevenloft_home_before_slider'); ?>


<section class="container-fluid slider-main">

	<div class="center owl-carousel">
		<?php
		if( have_rows('main_slider') ):
			while ( have_rows('main_slider') ) : the_row(); ?>
				<div class="hero__image" style="background-image: url(<?php  the_sub_field('image'); ?>);"></div>


			<?php endwhile;
		else :

		endif;
		?>
	</div> 
</section>

<?php do_action('sevenloft_home_after_slider'); ?>


<section class="container top-images">
	<div class="row">
		
		<div class="col-md-4 col-12">
			<div class="imgs-main-top">
				<a href="<?php echo get_field('img-top-left_link'); ?>">
					<div class="imgs-main-top-in" style="background-image: url(<?php echo get_field('image_top_left'); ?>);"></div>
				</a>
				<span class="text-img">DEINA</span>
			</div>
		</div>
		<div class="col-md-4 col-12">
			<div class="imgs-main-top">
				<a href="<?php echo get_field('image_top_middle_link'); ?>">
					<div class="imgs-main-top-in" style="background-image: url(<?php echo get_field('image_top_middle'); ?>);"></div>
				</a>
				<!-- <span class="text-img">ΘΗΚΕΣ</span> -->
			</div>
		</div>
		<div class="col-md-4 col-12">
			<div class="imgs-main-top">
				<a href="<?php echo get_field('image_top_right_link'); ?>">
					<div class="imgs-main-top-in" style="background-image: url(<?php echo get_field('image_top_right'); ?>);"></div>
				</a>
				<span class="text-img">ΕΓΓΡΑΦΗ B2B</span>
			</div>
		</div>
	</div>

</section>

<?php do_action('sevenloft_home_after_bottomslider'); ?>


<?php 
if ( dynamic_sidebar('Home Section 1') ) : 
else : 
?>
<?php endif; ?>


<?php do_action('sevenloft_home_before_homesection2'); ?>


<?php 
if ( dynamic_sidebar('Home Section 2') ) : 
else : 
?>
<?php endif; ?>

<?php do_action('sevenloft_home_before_homesection3'); ?>

<?php 
if ( dynamic_sidebar('Home Section 3') ) : 
else : 
?>
<?php endif; ?>

<?php do_action('sevenloft_home_before_homesection4'); ?>

<?php 
if ( dynamic_sidebar('Home Section 4') ) : 
else : 
?>
<?php endif; ?>

<?php do_action('sevenloft_home_before_homesection5'); ?>

<?php 
if ( dynamic_sidebar('Home Section 5') ) : 
else : 
?>
<?php endif; ?>

<?php do_action('sevenloft_home_before_homesection6'); ?>

<?php 
if ( dynamic_sidebar('Home Section 6') ) : 
else : 
?>
<?php endif; ?>

<?php do_action('sevenloft_home_before_homesection7'); ?>

<?php 
if ( dynamic_sidebar('Home Section 7') ) : 
else : 
?>
<?php endif; ?>

<section class="featured_categories container">
	<div class="dflexboxes">

		<div class="fet_title">
			<span class="fet-text">Επιλεγμένες κατηγορίες</span>
			<span class="fet-text-small">Ανακάλυψε τις πιο δημοφιλής κατηγορίες</span>
		</div>
		<div class="fet_title_all">
			<div class="fet_in">
				<?php 
					$args = array( 'post_type' => 'product', 'post_status' => 'publish', 
					'posts_per_page' => -1 );
					$products = new WP_Query( $args ); ?>
				<span class="tcount"><?php echo $products->found_posts; ?></span>
				<span class="tcount-title">Προϊόντα</span>
			</div>
			<div class="fet_in">
				<span class="tcount">120</span>
				<span class="tcount-title">Κατηγορίες</span>
			</div>
		</div>
	</div>

	<div class="row dflexboxes2">

		<?php
			$args = array(
			    'number'     => $number,
			    'orderby'    => $orderby,
			    'order'      => $order,
			    'hide_empty' => $hide_empty,
			    'include'    => $ids,
			    'parent' => 0
			);

			$product_categories = get_terms( 'product_cat', $args );

			foreach( $product_categories as $cat )  { 

			$thumbnail_id = get_woocommerce_term_meta($cat->term_id, 'thumbnail_id', true);
        $image = wp_get_attachment_url($thumbnail_id);
				$link = get_term_link($cat->term_id);
        ?>
     
		   
				<div class="category_box myBox">
					<!-- <?php echo "<img class='img-fluid' src='{$image}' alt='category'  />"; ?> -->
					<?php echo "<img class='img-fluid' src='http://demo.lmcompany.gr/wp-content/uploads/2020/05/Computer-Laptop-category.jpg' alt='category'  />"; ?>
					<a href="<?php echo $link ?>">
						<h1><?php echo $cat->name; ?></h1>
					</a>
					<span><?php echo $cat->count; ?> Προϊόντα </span>
				</div>
			   
			<?php			
			}
		?>
		

	</div>
</section>

<section class="home-tabs">
	<div class="container">
		<span class="protin">ΠΡΟΤΕΙΝΟΜΕΝΑ ΠΡΟΪΟΝΤΑ</span>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-12">
				<div class="featured-products-">
					<div id="tabs" class="text-center"> 

						<div class="tab-content clearfix">
							<div class="tab-pane active" id="tab_new">
								<?php $cform2 = __('[featured_products per_page="8"]','sevenloft-eshop'); ?>
								<?php echo do_shortcode( $cform2 ); ?>
							</div>
							
						</div>
					</div> 
				</div>
			</div>
		</div>
	</div>
</section>

<?php do_action('sevenloft_home_after_featured_products'); ?>


<section class="home-tabs">
	<div class="container">
		<span class="protin">ΤΕΛΕΥΤΑΙΑ ΠΡΟΪΟΝΤΑ</span>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-12">
				<div class="featured-products-">
					<div id="tabs" class="text-center"> 

						<div class="tab-content clearfix">
							<div class="tab-pane active" id="tab_new">
								<?php $cform2 = __('[products limit="8" orderby="id" order="DESC" visibility="visible"]
','sevenloft-eshop'); ?>
								<?php echo do_shortcode( $cform2 ); ?>
							</div>
							
						</div>
					</div> 
				</div>
			</div>
		</div>
	</div>
</section>






<?php do_action('sevenloft_home_after_latest_products'); ?>

<?php get_footer(); ?>




