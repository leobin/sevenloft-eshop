<?php
/*Template Name: Contact */
?>

<?php get_header(); ?>

<script src="https://maps.googleapis.com/maps/api/js?sensor=false&key=AIzaSyB-N1wGoOaHgfhHf8YyHN98pyZ5GQYRwpw"></script>

<?php if(have_posts()) : ?>
  <?php while(have_posts()) : the_post(); ?>
<section class="contact-page">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-12">
             <h1 class="page-title">
                <?php the_title(); ?>
              </h1>
              <h2><?php _e( 'Επικοινωνήστε μαζί μας συμπληρώνοντας την φόρμα επικοινωνίας.', 'sevenloft-eshop' ); ?></h2>
              <p><?php _e( 'Κάποιος εκπρόσωπος μας θα αναλάβει να σας απαντήσει άμεσα γι’ αυτό είναι απαραίτητη η σωστή εισαγωγή των στοιχείων σας.', 'sevenloft-eshop' ); ?></p>
              <div class="row">
                <div class="col-md-6 col-sm-6 col-12">
                  <div class="map">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3027.5287941581423!2d22.93124161565378!3d40.64027254993693!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14a8399cbcca50ff%3A0x9b2efc9d4f840d85!2sLM%20Company!5e0!3m2!1sel!2sgr!4v1588836173153!5m2!1sel!2sgr" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                  </div>
                </div>
                <div class="col-md-6 col-sm-6 col-12">
                  <p class="contact-info"><?php _e( '<strong>ΤΗΛΕΦΩΝΙΚΟ ΚΕΝΤΡΟ</strong>:<br/>
                    Τηλέφωνο: 2310305312 <br/>
                    fax. 2310 324255<br/><br/>
                    <strong>ΕΔΡΑ ΚΑΤΑΣΤΗΜΑΤΟΣ</strong></br>
                    Διεύθυνση: Γ. Αγγέλου & Νεάρχου 1 <br/>
                    ΤΚ: 54250<br/>
                    Θεσσαλονίκη <br/><br/>

                  <strong>ΩΡΑΡΙΟ ΕΞΥΠΗΡΕΤΗΣΗΣ ΠΕΛΑΤΩΝ</strong>:<br/>
                  Δευτέρα εώς Παρασκευή - 08:00 με 15:00 και 17.30 με 21.00<br/>
                  Σάββατο - 08:00 με 15:00<br/><br/>
                  <a class="color-black" href="mailto:info@toolman.gr">info@toolman.gr</a>', 'sevenloft-eshop' ); ?>
                </p>                 
                </div>
              </div>
        </div>
      </div>
  </div>
</section>
    
  <?php endwhile; ?>

<?php else : ?>

<h2>
  <?php _e( 'H σελίδα δεν βρέθηκε', 'sevenloft-eshop' ); ?>
</h2>

<?php endif; ?>



<!-- footer of mywebdesign -->

<?php get_footer(); ?>

