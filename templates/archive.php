
<?php get_header(); ?>
<section class="articles-page">
	<div class="container">
  		<div class="row">



<?php global $query_string; query_posts($query_string . "&order=DESC"); ?>

  <?php if(have_posts()) : ?>  
    <?php while(have_posts()) : the_post(); ?>  

				<div class="col-md-4 col-sm-4 col-12">
					<div class="home-news">
						<div class="home-news-image">
							<?php the_post_thumbnail('full', array(
							'alt' => 'no alt',
							'class' => 'img-responsive news-list-img',
							'title' => '' . get_the_title() . ''
							)); ?>
						</div>					
						<div class="home-news-category">
							<span class="home-news-category-label"></span>
						</div>
						<div class="home-news-title">
							<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
						</div>		
						<div class="home-news-description">
							<!-- <?php the_excerpt(); ?> -->
							<?php //echo types_render_field("shortdescr", array("arg1"=>"val1")) ?>
						</div>	
						<div class="home-news-more">
							<a href="<?php the_permalink(); ?>"><?php _e( 'Διαβάστε περισσότερα...', 'sevenloft-eshop' ); ?></a>
						</div>					

					</div>
				</div>


      <?php endwhile; 
      wp_reset_postdata(); ?>

  
  <?php else : ?>

	<div class='notfound'>
	  <h2>
	   <?php _e( 'Η σελίδα που ζητήσατε δεν βρέθηκε.', 'sevenloft-eshop' ); ?>
	  </h2>

	  <p> <?php _e( 'Η σελίδα που αναζητήσατε μπορεί να έχει διαγραφεί, να έχει μετονομαστεί ή να μην είναι διαθέσιμη. Παρακαλούμε, πατήστε <a href="/">εδώ</a> έτσι ώστε να οδηγηθείτε στην αρχική σελίδα.', 'sevenloft-eshop' ); ?></p>
	</div>
  <?php endif; ?>

		</div>
	</div>
</section>

<?php get_footer(); ?>
