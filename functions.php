<?php

/* Για λόγους ασφάλειας να μην επιτρέπεται η απευθείας πρόσβαση στο αρχείο */
if ( ! defined( 'ABSPATH' ) ) {
  exit;
}

/* Προσθήκη Wordpress Functions */
require 'inc/core/wordpress-functions.php';

/* Προσθήκη Woocommerce Functions */
require 'inc/core/woocommerce-functions.php';

/* Προσθήκη εγκατάστασης για Required Plugins */
require_once get_template_directory() . '/inc/core/class-tgm-plugin-activation.php';
add_action( 'tgmpa_register', 'sevenloft_eshop_register_required_plugins' );

/* Προσθήκη Ρυθμίσεων του Template Functions */
require 'inc/core/admin-functions.php';

/* Προσθήκη Wordpress Widget Functions */
require 'inc/widgets/wordpress-widgets.php';

/* Προσθήκη Woocommerce Widget Functions */
require 'inc/widgets/woocommerce-widgets.php';

/* Προσθήκη Widget για προιόντα */
require 'inc/widgets/class-wc-widget-products.php';

function add_theme_scripts()
{
  //Styles
  wp_enqueue_style('default-fonts', 'https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&amp;subset=greek', array(), time());
  wp_enqueue_style('boostrap-css', get_template_directory_uri() . '/assets/bootstrap/css/bootstrap.min.css', array(), time());
  wp_enqueue_style('font-awesome', get_template_directory_uri() . '/assets/css/fontawesome.min.css', array(), time());
  wp_enqueue_style('slick', get_template_directory_uri() . '/assets/js/slick/slick.css', array(), time());
  wp_enqueue_style('slick-theme', get_template_directory_uri() . '/assets/js/slick/slick-theme.css', array(), time());
  wp_enqueue_style('slick-lightbox', get_template_directory_uri() . '/assets/js/slick/slick-lightbox.css', array(), time());
  wp_enqueue_style('owl-carousel', get_template_directory_uri() . '/assets/js/owlslider/owl.carousel.min.css', array(), time());
  wp_enqueue_style('theme-style', get_template_directory_uri() . '/style.css', array(), time());
  //Scripts
  wp_enqueue_script('poppers-js', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.6.0/umd/popper.min.js');
  wp_enqueue_style('font-awesome', get_template_directory_uri() . '/assets/bootstrap/js/bootstrap.min.js', array(), time());
  wp_enqueue_script('slick-js', get_template_directory_uri() .'/assets/js/slick/slick.js', array(), time());
  wp_enqueue_script('slick-lightbox-js', get_template_directory_uri() .'/assets/js/slick/slick-lightbox.min.js', array(), time());
  wp_enqueue_script('owl-carousel-js', get_template_directory_uri() .'/assets/js/owlslider/owl.carousel.min.js', array(), time());
  wp_enqueue_script('hover-intent-js', get_template_directory_uri() .'/assets/js/jquery.hoverIntent.min.js', array(), time());
  wp_enqueue_script('theme-js', get_template_directory_uri() .'/assets/js/func.js', array(), time());

}
add_action('wp_enqueue_scripts', 'add_theme_scripts');

function get_custom_post_type_template($single) {
  
  global $post;

  if ($post->post_type == 'product') {
       $single_template = dirname( __FILE__ ) . '/template-parts/single-template.php';
  }
  return $single_template;   
}
add_filter( 'single_template', 'get_custom_post_type_template' );

function sevenloft_page_template( $template ) 
{
    if( is_home() || is_front_page())
    {
      $new_template = locate_template( array( '/templates/justhome.php' ) );

      if ( '' != $new_template ) {
          return $new_template ;
      }
    }
    
    if ( is_archive() )
    {
      $new_template = '';
      global $wp_query;

      $cat = $wp_query->get_queried_object();
      //να γίνει δυναμικό ώστε να τραβάει αυτόματα τα paths από τις συσχετίσεις taxonomy - file name
      if($cat->taxonomy == "product_cat")
      {
        $new_template = locate_template( array( '/template-parts/archive-product.php' ) );
      }
      else
      {
        $new_template = locate_template( array( '/templates/archive.php' ) );
      }
      

      if ( '' != $new_template ) {
          return $new_template ;
      }
    }
    
    if ( is_category() )
    {
      $new_template = locate_template( array( '/templates/archive.php' ) );

      if ( '' != $new_template ) {
          return $new_template ;
      }
    }
    
    if ( is_single() )
    {

    }
    
    if ( is_404() )
    {

    }
    
    
    return $template;
}
add_filter( 'template_include', 'sevenloft_page_template', 99 );

?>