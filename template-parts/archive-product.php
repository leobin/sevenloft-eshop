<?php get_header(); ?>

<?php

if ( is_product_category() ){
  global $wp_query;

  // get the query object
  $cat = $wp_query->get_queried_object();
  $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'product_cat' ) ); 

  if ($term != null) {
  echo $term->name; 
  }

  $backgroundimage = wp_get_attachment_image_src( get_field('background_image', $term), 'full' )[0];

  if ($backgroundimage != null) {
 ?>
 <div class="category-header" style="background-image: url(<?php echo $backgroundimage; ?>)">

 </div>

<?php 
}
} ?>



  <?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) :  ?>
    
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-12">
          <h1 class="page-title"><?php woocommerce_page_title(); ?></h1>                  
          <?php endif; ?>
          <?php
          // verify that this is a product category page
          if ( is_product_category() ){
            global $wp_query;

            // get the query object
            $cat = $wp_query->get_queried_object();
          }?>

          <?php
          /**
          * woocommerce_archive_description hook
          *
          * @hooked woocommerce_taxonomy_archive_description - 10
          * @hooked woocommerce_product_archive_description - 10
          */
          do_action( 'woocommerce_archive_description' );

          ?>
      </div>
    </div>
  </div>
  
<section class="product-list">
  <div class="container">
    <div class="row">

      <div class="col-12 col-md-6 breadcrumb-wr">
          <?php
            /**
            * woocommerce_before_main_content hook
            *
            * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
            * @hooked woocommerce_breadcrumb - 20
            */
            do_action( 'woocommerce_before_main_content' );
          ?>
      </div>
     
      </div> 
      <div class="col-md-6 col-12 dfl-f"> 
        <div class="orderbyk">
                  
          <?php get_template_part( 'template-parts/sidebar-orderby' ); ?>
             
        </div>
        <div class="orderbyk2">
                  
          <?php get_template_part('template-parts/sidebar-paging' ); ?>
             
        </div>
      </div>
      </div> 
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
           
               <a href="javascript:void(0)" class="filters_button">Επιλογή Φίλτρων</a>
          </div>
        </div>
    <div class="row">
        <div class="col-md-3 col-12 prod-side">   

          <div class="sidebar-filter-wr category<?php echo $Root_Cat_ID; ?>">
            <div class="sidebar-filter">
              <?php 
              get_template_part ( 'template-parts/sidebar-shop_left' );
               ?>
            </div>
          </div>           
        </div>

        <div class="col-md-12 col-12 prod-list-main-wr prod-main colz-4 ">
          <div class="product-list-main">
                 <?php if ( have_posts() ) : ?>

                <?php woocommerce_product_loop_start(); ?>

                <?php woocommerce_product_subcategories(); ?>

                <?php while ( have_posts() ) : the_post(); ?>
                  <?php 
                  // is_featured();
                  wc_get_template_part( 'content', 'product' ); ?>
                <?php endwhile; // end of the loop. ?>

                <?php woocommerce_product_loop_end(); ?>

                <?php
                  /**
                  * woocommerce_after_shop_loop hook
                  *
                  * @hooked woocommerce_pagination - 10
                  */
                  do_action( 'woocommerce_after_shop_loop' );
                ?>

                <?php elseif ( ! woocommerce_product_subcategories( array( 
                'before' => woocommerce_product_loop_start( false ), 
                'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

                  <?php wc_get_template( 'loop/no-products-found.php' ); ?>

                <?php endif; ?>

                <?php
                  /**
                  * woocommerce_after_main_content hook
                  *
                  * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
                  */
                  //do_action( 'woocommerce_after_main_content' );
                ?>

                <?php
                  /**
                  * woocommerce_sidebar hook
                  *
                  * @hooked woocommerce_get_sidebar - 10
                  */
                  //do_action( 'woocommerce_sidebar' );
                ?>


            </div>
        </div>   
    </div> 
   
  </div>
</section>
<script type="text/javascript">

var urlParams = new URLSearchParams(window.location.search); //get all parameters
var material = urlParams.get('pa_material');
var collection = urlParams.get('pa_collection');
var size = urlParams.get('pa_size');
var price = urlParams.get('rng_min_price');

if(material || collection || size || price) { //check if foo parameter is set to anything
    jQuery( ".filters_button" ).removeClass( "act" );
            jQuery( ".prod-side" ).show();
        jQuery( ".prod-main" ).removeClass( "col-md-12 col-xs-12" );
        jQuery( ".prod-main" ).addClass( "col-md-9 col-xs-12" );
        jQuery( ".prod-main" ).removeClass( "colz-4" );
        jQuery( ".prod-item" ).removeClass( "col-md-3 col-sm-6 col-xs-12" );
        jQuery( ".prod-item" ).addClass( "col-md-4 col-sm-6 col-xs-12" );
        jQuery( ".filters_button" ).addClass( "act" );
}





   jQuery( ".filters_button" ).click(function() {

      if ($( ".filters_button" ).hasClass( "act" )) {
        jQuery( ".prod-side" ).hide();  
        jQuery( ".prod-main" ).removeClass( "col-md-9 col-xs-12" );
        jQuery( ".prod-main" ).addClass( "col-md-12 col-xs-12" );
        jQuery( ".prod-main" ).addClass( "colz-4" );
        jQuery( ".prod-item" ).removeClass( "col-md-4 col-sm-6 col-xs-12" );
        jQuery( ".prod-item" ).addClass( "col-md-3 col-sm-6 col-xs-12" );
        jQuery( ".filters_button" ).removeClass( "act" );
      }
      else {
        jQuery( ".prod-side" ).show();
        jQuery( ".prod-main" ).removeClass( "col-md-12 col-xs-12" );
        jQuery( ".prod-main" ).addClass( "col-md-9 col-xs-12" );
        jQuery( ".prod-main" ).removeClass( "colz-4" );
        jQuery( ".prod-item" ).removeClass( "col-md-3 col-sm-6 col-xs-12" );
        jQuery( ".prod-item" ).addClass( "col-md-4 col-sm-6 col-xs-12" );
        jQuery( ".filters_button" ).addClass( "act" );
  
      }
   });

  
   //
</script>
<?php get_footer(); ?>