<?php
/*Template Name: Sale Page */
?>


<?php get_header(); ?>


<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) :  ?>
<div class="container">
   <div class="row">
      <div class="col-md-12 col-sm-12 col-12">
         <h1 class="page-title"><?php _e( 'Προσφορές', 'sevenloft-eshop' ); ?></h1>
         <?php endif; ?>
         <?php
            // verify that this is a product category page
            if ( is_product_category() ){
              global $wp_query;
            
              // get the query object
              $cat = $wp_query->get_queried_object();
            }?>
         <?php
            /**
            * woocommerce_archive_description hook
            *
            * @hooked woocommerce_taxonomy_archive_description - 10
            * @hooked woocommerce_product_archive_description - 10
            */
            do_action( 'woocommerce_archive_description' );
            
            ?>
      </div>
   </div>
</div>
<section class="product-list">
   <div class="container">
      <div class="row">
         <div class="col-12 col-md-6 breadcrumb-wr">
            <?php
               /**
               * woocommerce_before_main_content hook
               *
               * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
               * @hooked woocommerce_breadcrumb - 20
               */
               do_action( 'woocommerce_before_main_content' );
               ?>
         </div>
      </div>
      <div class="col-md-6 col-12 dfl-f">
         <div class="orderbyk">
            <?php get_sidebar( 'orderby' ); ?>
         </div>
         <div class="orderbyk2">
            <?php get_sidebar( 'paging' ); ?>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
         <a href="javascript:void(0)" class="filters_button"><?php _e( 'Επιλογή Φίλτρων', 'sevenloft-eshop' ); ?></a>
      </div>
   </div>
   <div class="row">
      <div class="col-md-3 col-12 prod-side">
         <div class="sidebar-filter-wr category<?php echo $Root_Cat_ID; ?>">
            <div class="sidebar-filter">
               <?php
                  get_sidebar( 'shop_left' );
                ?>
            </div>
         </div>
      </div>
      <div class="col-md-12 col-12 prod-list-main-wr prod-main colz-4 ">
         <div class="product-list-main">
          <?php 
          $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
          $query_args = array(
              'posts_per_page'    => 9,
              'post_status'       => 'publish',
              'post_type'         => 'product',
              'meta_query'        => WC()->query->get_meta_query(),
              'post__in'          => array_merge( array( 0 ), wc_get_product_ids_on_sale() ),
              'paged' => $paged,
          );
          $loop = new WP_Query( $query_args );

          if ( $loop->have_posts() ) : 

          woocommerce_product_loop_start();
           while ( $loop->have_posts() ) : $loop->the_post(); ?>
            
            <?php 
               // is_featured();
               wc_get_template_part( 'content', 'product' ); ?>
            <?php endwhile; // end of the loop. ?>

            <?php
            echo '';
            $total_pages = $loop->max_num_pages;

              if ($total_pages > 1){

                  $current_page = max(1, get_query_var('paged'));
                  echo '<nav class="woocommerce-pagination-sale">';
                  echo paginate_links(array(
                      'base' => get_pagenum_link(1) . '%_%',
                      'format' => 'page/%#%/',
                      'current' => $current_page,
                      'total' => $total_pages,
                      'prev_text' => '←',
                      'next_text' => '→',
                  ));
                  echo '</nav>';
              }    
          
         
          ?>
            <?php woocommerce_product_loop_end(); ?>
            <?php
               /**
               * woocommerce_after_shop_loop hook
               *
               * @hooked woocommerce_pagination - 10
               */
               do_action( 'woocommerce_after_shop_loop' );
               ?>
            <?php elseif ( ! woocommerce_product_subcategories( array( 
               'before' => woocommerce_product_loop_start( false ), 
               'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>
            <?php wc_get_template( 'loop/no-products-found.php' ); ?>
            <?php endif;
             wp_reset_postdata();
             ?>
            <?php
               /**
               * woocommerce_after_main_content hook
               *
               * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
               */
               //do_action( 'woocommerce_after_main_content' );
               ?>
            <?php
               /**
               * woocommerce_sidebar hook
               *
               * @hooked woocommerce_get_sidebar - 10
               */
               //do_action( 'woocommerce_sidebar' );
               ?>
         </div>
      </div>
   </div>
   </div>
</section>
<script>

  function setmaxHeight(){

    $('.products .product').each(function(){
        $(this).css('height','auto');
    });

    $('.products .product-item').each(function(){
        $(this).css('height','auto');
    });

    console.log('resize auto');

    
    var maxHeight = 0;

    $('.products .product').each(function(){
        if ($(this).height() > maxHeight) { maxHeight = $(this).height(); }
    });

    $('.products .product').height(maxHeight);

    var maxHeight2 = 0;

    $('.products .product-item').each(function(){
       if ($(this).height() > maxHeight2) { maxHeight2 = $(this).height(); }
    });

    $('.products .product-item').height(maxHeight2);

}


   var urlParams = new URLSearchParams(window.location.search); //get all parameters
   var material = urlParams.get('pa_material');
   var collection = urlParams.get('pa_collection');
   var size = urlParams.get('pa_size');
   var price = urlParams.get('rng_min_price');

   
     if(material || collection || size || price) { //check if foo parameter is set to anything
         jQuery( ".filters_button" ).removeClass( "act" );
             jQuery( ".prod-side" ).show();
             jQuery( ".prod-main" ).removeClass( "col-md-12 col-xs-12" );
             jQuery( ".prod-main" ).addClass( "col-md-9 col-xs-12" );
             jQuery( ".prod-main" ).removeClass( "colz-4" );
             jQuery( ".prod-item" ).removeClass( "col-md-3 col-sm-6 col-xs-12" );
             jQuery( ".prod-item" ).addClass( "col-md-4 col-sm-6 col-xs-12" );
             jQuery( ".filters_button" ).addClass( "act" );
     }
   
      jQuery( ".filters_button" ).click(function() {


   
         if ($( ".filters_button" ).hasClass( "act" )) {
           jQuery( ".prod-side" ).hide();  
           jQuery( ".prod-main" ).removeClass( "col-md-9 col-xs-12" );
           jQuery( ".prod-main" ).addClass( "col-md-12 col-xs-12" );
           jQuery( ".prod-main" ).addClass( "colz-4" );
           jQuery( ".prod-item" ).removeClass( "col-md-4 col-sm-6 col-xs-12" );
           jQuery( ".prod-item" ).addClass( "col-md-3 col-sm-6 col-xs-12" );
           jQuery( ".filters_button" ).removeClass( "act" );
         }
         else {
           jQuery( ".prod-side" ).show();
           jQuery( ".prod-main" ).removeClass( "col-md-12 col-xs-12" );
           jQuery( ".prod-main" ).addClass( "col-md-9 col-xs-12" );
           jQuery( ".prod-main" ).removeClass( "colz-4" );
           jQuery( ".prod-item" ).removeClass( "col-md-3 col-sm-6 col-xs-12" );
           jQuery( ".prod-item" ).addClass( "col-md-4 col-sm-6 col-xs-12" );
           jQuery( ".filters_button" ).addClass( "act" );

     
         }

        setmaxHeight();


      });
   
     
      //
</script>
<?php get_footer(); ?>

