<?php
/*Template Name: Simple Page */
?>

<?php get_header(); ?>

<?php if(have_posts()) : ?>
  <?php while(have_posts()) : the_post(); ?>

    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-12">
          <div class="simple-page-section">
             <h1 class="simple-page-title"><?php the_title(); ?></h1>
              <?php the_content(''); ?>
          </div>
        </div>
      </div>
    </div>

    
  <?php endwhile; ?>

<?php else : ?>

<h2>
  <?php _e( 'H σελίδα δεν βρέθηκε', 'sevenloft-eshop' ); ?>
</h2>

<?php endif; ?>


<?php get_footer(); ?>