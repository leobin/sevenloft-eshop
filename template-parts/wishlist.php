<?php
/*Template Name: Wishlist */
?>

<?php get_header(); ?>

<?php if(have_posts()) : ?>
  <?php while(have_posts()) : the_post(); ?>
<section class="contact-page">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-12">
             <h1 class="page-title">
                <?php the_title(); ?>
              </h1>
         		
        </div>
      </div>
  </div>
</section>
    
  <?php endwhile; ?>

<?php else : ?>

<h2>
  <?php _e( 'H σελίδα δεν βρέθηκε', 'sevenloft-eshop' ); ?>
</h2>

<?php endif; ?>

<?php get_footer(); ?>