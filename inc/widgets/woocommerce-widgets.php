<?php


 /* Προσθήκη σημείων για widgets */

 
  function arphabet_widgets_init() {

    register_sidebar( array(
        'name' => 'Filters',
        'id' => 'filters',
        'before_widget' => '<div>',
        'after_widget' => '</div>',
        'before_title' => '<div class="filters_in">',
        'after_title' => '</div>',
    ) );

    register_sidebar( array(
        'name' => 'Home Section 1',
        'id' => 'homesection1',
        'before_widget' => '<div>',
        'after_widget' => '</div>',
        'before_title' => '<div class="home_section_1">',
        'after_title' => '</div>',
    ) );
  
    register_sidebar( array(
        'name' => 'Home Section 2',
        'id' => 'homesection2',
        'before_widget' => '<div>',
        'after_widget' => '</div>',
        'before_title' => '<div class="home_section_2">',
        'after_title' => '</div>',
    ) );

    register_sidebar( array(
        'name' => 'Home Section 3',
        'id' => 'homesection3',
        'before_widget' => '<div>',
        'after_widget' => '</div>',
        'before_title' => '<div class="home_section_3">',
        'after_title' => '</div>',
    ) );
  
    register_sidebar( array(
        'name' => 'Home Section 4',
        'id' => 'homesection4',
        'before_widget' => '<div>',
        'after_widget' => '</div>',
        'before_title' => '<div class="home_section_4">',
        'after_title' => '</div>',
    ) );


    register_sidebar( array(
        'name' => 'Home Section 5',
        'id' => 'homesection5',
        'before_widget' => '<div>',
        'after_widget' => '</div>',
        'before_title' => '<div class="home_section_5">',
        'after_title' => '</div>',
    ) );


    register_sidebar( array(
        'name' => 'Home Section 6',
        'id' => 'homesection6',
        'before_widget' => '<div>',
        'after_widget' => '</div>',
        'before_title' => '<div class="home_section_6">',
        'after_title' => '</div>',
    ) );


    register_sidebar( array(
        'name' => 'Home Section 7',
        'id' => 'homesection7',
        'before_widget' => '<div>',
        'after_widget' => '</div>',
        'before_title' => '<div class="home_section_7">',
        'after_title' => '</div>',
    ) );

    register_sidebar( array(
          'name' => 'Search_Bar',
          'id' => 'Search_Bar',
          'before_widget' => '<div>',
          'after_widget' => '</div>',
          'before_title' => '<div class="search_bar">',
          'after_title' => '</div>',
      ) );

     register_sidebar( array(
          'name' => 'Paging',
          'id' => 'paging',
          'before_widget' => '<div>',
          'after_widget' => '</div>',
          'before_title' => '<div class="paging">',
          'after_title' => '</div>',
      ) );

      register_sidebar( array(
          'name' => 'Order By',
          'id' => 'order_by',
          'before_widget' => '<div>',
          'after_widget' => '</div>',
          'before_title' => '<div class="order_by">',
          'after_title' => '</div>',
      ) );



  }



add_action( 'widgets_init', 'arphabet_widgets_init' );



?>