<?php

/* Wordpress */

/* Γενικά */

/* Ορίζουμε το ωφέλιμο width του template */

if ( ! isset( $content_width ) )
    $content_width = 980; /* pixels */


/* Δημιουργία jira service desk support ticket για κάθε error του site */

add_filter( 'recovery_mode_email', function( $email_data ) {
    $email_data['to'] = 'jira@sevenloft.atlassian.net';
    return $email_data;
});

/* Εγκατάσταση των plugin που απαιτούνται */

function sevenloft_eshop_register_required_plugins() {
  /*
   * Array of plugin arrays. Required keys are name and slug.
   * If the source is NOT from the .org repo, then source is also required.
   */
  $plugins = array(

    // This is an example of how to include a plugin bundled with a theme.
    /*array(
      'name'               => 'TGM Example Plugin', // The plugin name.
      'slug'               => 'tgm-example-plugin', // The plugin slug (typically the folder name).
      'source'             => get_template_directory() . '/lib/plugins/tgm-example-plugin.zip', // The plugin source.
      'required'           => true, // If false, the plugin is only 'recommended' instead of required.
      'version'            => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.
      'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
      'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
      'external_url'       => '', // If set, overrides default API URL and points to an external URL.
      'is_callable'        => '', // If set, this callable will be be checked for availability to determine if a plugin is active.
    ), */

    // This is an example of how to include a plugin from an arbitrary external source in your theme.
    array(
      'name'         => 'Advanced Custom Fields PRO', // The plugin name.
      'slug'         => 'advanced-custom-fields-pro', // The plugin slug (typically the folder name).
      'source'       => 'https://www.sevenloft.gr/framework/advanced-custom-fields-pro.zip', // The plugin source.
      'required'     => true, // If false, the plugin is only 'recommended' instead of required.
      //'external_url' => 'https://github.com/thomasgriffin/New-Media-Image-Uploader', // If set, overrides default API URL and points to an external URL.
    ),

    array(
      'name'         => 'Sevenloft Plugin', // The plugin name.
      'slug'         => 'sevenloft-plugin', // The plugin slug (typically the folder name).
      'source'       => 'https://www.sevenloft.gr/framework/sevenloft-plugin.zip', // The plugin source.
      'required'     => true, // If false, the plugin is only 'recommended' instead of required.
      //'external_url' => 'https://github.com/thomasgriffin/New-Media-Image-Uploader', // If set, overrides default API URL and points to an external URL.
    ),

    array(
      'name'         => 'Woocommerce Cart Pro', // The plugin name.
      'slug'         => 'woocommerce-cart-pro', // The plugin slug (typically the folder name).
      'source'       => 'https://www.sevenloft.gr/framework/woocommerce-cart-pro.zip', // The plugin source.
      'required'     => true, // If false, the plugin is only 'recommended' instead of required.
      //'external_url' => 'https://github.com/thomasgriffin/New-Media-Image-Uploader', // If set, overrides default API URL and points to an external URL.
    ),

    array(
      'name'         => 'Woocommerce Advanced Bulk Edit', // The plugin name.
      'slug'         => 'woocommerce-advanced-bulk-edit', // The plugin slug (typically the folder name).
      'source'       => 'https://www.sevenloft.gr/framework/woocommerce-advanced-bulk-edit.zip', // The plugin source.
      'required'     => true, // If false, the plugin is only 'recommended' instead of required.
      //'external_url' => 'https://github.com/thomasgriffin/New-Media-Image-Uploader', // If set, overrides default API URL and points to an external URL.
    ),

    array(
      'name'         => 'Ultimate GDPR', // The plugin name.
      'slug'         => 'ct-ultimate-gdpr', // The plugin slug (typically the folder name).
      'source'       => 'https://www.sevenloft.gr/framework/ct-ultimate-gdpr.zip', // The plugin source.
      'required'     => true, // If false, the plugin is only 'recommended' instead of required.
      //'external_url' => 'https://github.com/thomasgriffin/New-Media-Image-Uploader', // If set, overrides default API URL and points to an external URL.
    ),

    array(
      'name'         => 'Sevenloft Plugin', // The plugin name.
      'slug'         => 'sevenloft-plugin', // The plugin slug (typically the folder name).
      'source'       => 'https://www.sevenloft.gr/framework/sevenloft-plugin.zip', // The plugin source.
      'required'     => true, // If false, the plugin is only 'recommended' instead of required.
      //'external_url' => 'https://github.com/thomasgriffin/New-Media-Image-Uploader', // If set, overrides default API URL and points to an external URL.
    ),

    // This is an example of how to include a plugin from a GitHub repository in your theme.
    // This presumes that the plugin code is based in the root of the GitHub repository
    // and not in a subdirectory ('/src') of the repository.
    /*array(
      'name'      => 'Adminbar Link Comments to Pending',
      'slug'      => 'adminbar-link-comments-to-pending',
      'source'    => 'https://github.com/jrfnl/WP-adminbar-comments-to-pending/archive/master.zip',
    ),
    */

    // This is an example of how to include a plugin from the WordPress Plugin Repository.
    array(
      'name'      => 'Woocommerce',
      'slug'      => 'woocommerce',
      'required'  => true,
    ), 

    array(
      'name'      => 'Rank Math SEO Plugin',
      'slug'      => 'seo-by-rank-math',
      'required'  => true,
    ), 

    array(
      'name'      => 'AJAX Search for WooCommerce',
      'slug'      => 'ajax-search-for-woocommerce',
      'required'  => true,
    ), 
    
    array(
      'name'      => 'TI WooCommerce Wishlist Plugin',
      'slug'      => 'ti-woocommerce-wishlist',
      'required'  => true,
    ), 



    // This is an example of the use of 'is_callable' functionality. A user could - for instance -
    // have WPSEO installed *or* WPSEO Premium. The slug would in that last case be different, i.e.
    // 'wordpress-seo-premium'.
    // By setting 'is_callable' to either a function from that plugin or a class method
    // `array( 'class', 'method' )` similar to how you hook in to actions and filters, TGMPA can still
    // recognize the plugin as being installed.
    /*array(
      'name'        => 'WordPress SEO by Yoast',
      'slug'        => 'wordpress-seo',
      'is_callable' => 'wpseo_init',
    ), */

  );

  /*
   * Array of configuration settings. Amend each line as needed.
   *
   * TGMPA will start providing localized text strings soon. If you already have translations of our standard
   * strings available, please help us make TGMPA even better by giving us access to these translations or by
   * sending in a pull-request with .po file(s) with the translations.
   *
   * Only uncomment the strings in the config array if you want to customize the strings.
   */
  $config = array(
    'id'           => 'sevenloft-eshop',                 // Unique ID for hashing notices for multiple instances of TGMPA.
    'default_path' => '',                      // Default absolute path to bundled plugins.
    'menu'         => 'tgmpa-install-plugins', // Menu slug.
    'parent_slug'  => 'themes.php',            // Parent menu slug.
    'capability'   => 'edit_theme_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
    'has_notices'  => true,                    // Show admin notices or not.
    'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
    'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
    'is_automatic' => false,                   // Automatically activate plugins after installation or not.
    'message'      => '',                      // Message to output right before the plugins table.

    /*
    'strings'      => array(
      'page_title'                      => __( 'Install Required Plugins', 'sevenloft-eshop' ),
      'menu_title'                      => __( 'Install Plugins', 'sevenloft-eshop' ),
      /* translators: %s: plugin name. * /
      'installing'                      => __( 'Installing Plugin: %s', 'sevenloft-eshop' ),
      /* translators: %s: plugin name. * /
      'updating'                        => __( 'Updating Plugin: %s', 'sevenloft-eshop' ),
      'oops'                            => __( 'Something went wrong with the plugin API.', 'sevenloft-eshop' ),
      'notice_can_install_required'     => _n_noop(
        /* translators: 1: plugin name(s). * /
        'This theme requires the following plugin: %1$s.',
        'This theme requires the following plugins: %1$s.',
        'sevenloft-eshop'
      ),
      'notice_can_install_recommended'  => _n_noop(
        /* translators: 1: plugin name(s). * /
        'This theme recommends the following plugin: %1$s.',
        'This theme recommends the following plugins: %1$s.',
        'sevenloft-eshop'
      ),
      'notice_ask_to_update'            => _n_noop(
        /* translators: 1: plugin name(s). * /
        'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.',
        'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.',
        'sevenloft-eshop'
      ),
      'notice_ask_to_update_maybe'      => _n_noop(
        /* translators: 1: plugin name(s). * /
        'There is an update available for: %1$s.',
        'There are updates available for the following plugins: %1$s.',
        'sevenloft-eshop'
      ),
      'notice_can_activate_required'    => _n_noop(
        /* translators: 1: plugin name(s). * /
        'The following required plugin is currently inactive: %1$s.',
        'The following required plugins are currently inactive: %1$s.',
        'sevenloft-eshop'
      ),
      'notice_can_activate_recommended' => _n_noop(
        /* translators: 1: plugin name(s). * /
        'The following recommended plugin is currently inactive: %1$s.',
        'The following recommended plugins are currently inactive: %1$s.',
        'sevenloft-eshop'
      ),
      'install_link'                    => _n_noop(
        'Begin installing plugin',
        'Begin installing plugins',
        'sevenloft-eshop'
      ),
      'update_link'             => _n_noop(
        'Begin updating plugin',
        'Begin updating plugins',
        'sevenloft-eshop'
      ),
      'activate_link'                   => _n_noop(
        'Begin activating plugin',
        'Begin activating plugins',
        'sevenloft-eshop'
      ),
      'return'                          => __( 'Return to Required Plugins Installer', 'sevenloft-eshop' ),
      'plugin_activated'                => __( 'Plugin activated successfully.', 'sevenloft-eshop' ),
      'activated_successfully'          => __( 'The following plugin was activated successfully:', 'sevenloft-eshop' ),
      /* translators: 1: plugin name. * /
      'plugin_already_active'           => __( 'No action taken. Plugin %1$s was already active.', 'sevenloft-eshop' ),
      /* translators: 1: plugin name. * /
      'plugin_needs_higher_version'     => __( 'Plugin not activated. A higher version of %s is needed for this theme. Please update the plugin.', 'sevenloft-eshop' ),
      /* translators: 1: dashboard link. * /
      'complete'                        => __( 'All plugins installed and activated successfully. %1$s', 'sevenloft-eshop' ),
      'dismiss'                         => __( 'Dismiss this notice', 'sevenloft-eshop' ),
      'notice_cannot_install_activate'  => __( 'There are one or more required or recommended plugins to install, update or activate.', 'sevenloft-eshop' ),
      'contact_admin'                   => __( 'Please contact the administrator of this site for help.', 'sevenloft-eshop' ),

      'nag_type'                        => '', // Determines admin notice type - can only be one of the typical WP notice classes, such as 'updated', 'update-nag', 'notice-warning', 'notice-info' or 'error'. Some of which may not work as expected in older WP versions.
    ),
    */
  );

  tgmpa( $plugins, $config );
}

/* Εφαρμόζουμε την σωστή έκδοση του jquery */

/*function my_init() {
   
  if (!is_admin()) {
    wp_deregister_script('jquery');
    wp_register_script('jquery', ("https://code.jquery.com/jquery-1.12.0.min.js"), false);
    wp_enqueue_script('jquery');
  }

}
add_action('init', 'my_init');*/

/* Απενεργοποιούμε την δυνατότητα emoji */

remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );

/* Απενεργοποιούμε το widget site health */

add_action('wp_dashboard_setup', 'remove_site_health_dashboard_widget');
function remove_site_health_dashboard_widget()
{
    remove_meta_box('dashboard_site_health', 'dashboard', 'normal');
}



/* Προσθήκη rss feed */

add_theme_support( 'automatic-feed-links' );


/* Υποστήριξη επιλογής τίτλου */

add_theme_support( "title-tag" );

/* Υποστήριξη επιλογής λογότυπου στο theme */

add_theme_support( 'custom-logo', array(
  'height'      => 100,
  'width'       => 400,
  'flex-height' => true,
  'flex-width'  => true,
  'header-text' => array( get_bloginfo( 'name' ) , get_bloginfo( 'description' ) ),
) );


/* Αλλαγή class name στο logo */

function change_logo_class( $html ) {

    $html = str_replace( 'custom-logo', 'logo', $html );
    $html = str_replace( 'custom-logo-link', 'logo-link', $html );

    return $html;
}

add_filter( 'get_custom_logo', 'change_logo_class' );


/* Υποστήριξη responsive menu */

function register_navwalker(){
  require_once get_template_directory() . '/inc/core/class-wp-bootstrap-navwalker.php';
}
add_action( 'after_setup_theme', 'register_navwalker' );



/* Προσθήκη των θέσεων για menu */
function register_my_menus()
{
      register_nav_menus(array(
          'primary' => __( 'Primary Menu', 'sevenloft-eshop' ),
          'footer1' => __( 'Footer Menu 1', 'sevenloft-eshop' ),
          'footer2' => __( 'Footer Menu 2', 'sevenloft-eshop' ),
          'footer3' => __( 'Footer Menu 3', 'sevenloft-eshop' ),
          'footer4' => __( 'Footer Menu 4', 'sevenloft-eshop' ),
      ));

}
add_action('init', 'register_my_menus');



/* Προσθήκη δυνατότητας featured image */

add_theme_support( "post-thumbnails" );


/* Προσθήκη δυνατότητας custom header και τίτλου σε κάθε σελίδα */

function sevenlofteshop_custom_header_setup() {
    $args = array(
        'default-image'      => get_template_directory_uri() . 'assets/images/default.jpg',
        'default-text-color' => '000',
        'width'              => 1000,
        'height'             => 250,
        'flex-width'         => true,
        'flex-height'        => true,
    );
    add_theme_support( 'custom-header', $args);
}
add_action( 'after_setup_theme', 'sevenlofteshop_custom_header_setup' );


/* Προσθήκη δυνατότητας χρώματος ή φωτογραφίας στο body */

add_theme_support( 'custom-background' );


/* Εμφάνιση του style στον visual editor στο διαχειριστικό */

add_action( 'init', 'cd_add_editor_styles' );
function cd_add_editor_styles() {
 add_editor_style( get_stylesheet_uri() );
}


/* Αφαίρεση των type = javascript για το w3c validator */
add_action(
    'after_setup_theme',
    function() {
        add_theme_support( 'html5', [ 'script', 'style' ] );
    }
);

?>