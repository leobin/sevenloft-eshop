<?php


/* Woocommerce */

/* Yποστήριξη Woocommerce */

add_theme_support( 'woocommerce' );

/* Λίστα προιόντων */

/* Εμφάνιση ποσοστού έκπτωσης */
 
add_action( 'woocommerce_before_shop_loop_item_title', 'bbloomer_show_sale_percentage_loop', 25 );
add_action( 'woocommerce_single_product_summary_title', 'bbloomer_show_sale_percentage_loop', 26 );

 
function bbloomer_show_sale_percentage_loop() {
 
global $product;
 
if ( $product->is_on_sale() ) {
 
  if ( ! $product->is_type( 'variable' ) ) {
    if($product->get_regular_price() != 0)
    {
      $max_percentage = ( ( $product->get_regular_price() - $product->get_sale_price() ) / $product->get_regular_price() ) * 100;
    }
    
    } else {
    
    $max_percentage = 0;
    
    foreach ( $product->get_children() as $child_id ) {
        $variation = wc_get_product( $child_id );
        $price = $variation->get_regular_price();
        $sale = $variation->get_sale_price();
        if ( $price != 0 && ! empty( $sale ) ) $percentage = ( $price - $sale ) / $price * 100;
        if ( $percentage > $max_percentage ) {
            $max_percentage = $percentage;
        }
    }
  
  }
  
  echo "<div class='sale-perc'>-" . round($max_percentage) . "%</div>";
  
  }
 
}

/* Breadcrumb */

add_filter( 'woocommerce_breadcrumb_defaults', 'custom_woocommerce_breadcrumbs' );
function custom_woocommerce_breadcrumbs() {
  return array(
    'delimiter'   => '', // no delimiter
    'wrap_before' => '<ul class="crumbs">',
    'wrap_after'  => '</ul>',
    'before'      => '<li>',
    'after'       => '</li>',
    'home'        => _x( 'ΑΡΧΙΚΗ', 'breadcrumb', 'woocommerce' ),
  );
}

/* Αναλυτική προιόντων */

/* Υποστήριξη zoom, lighbox και slider για την αναλυτική */

add_theme_support( 'wc-product-gallery-zoom' );
add_theme_support( 'wc-product-gallery-lightbox' );
add_theme_support( 'wc-product-gallery-slider' );


/* Αλλάζει την σειρά των elements στην αναλυτική */

remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_title',5);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price',10);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt',20);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart',30);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta',40);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_sharing',50);

add_action('woocommerce_single_product_summary', 'woocommerce_template_single_title',5);
add_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta',10);
add_action('woocommerce_single_product_summary', 'woocommerce_template_single_price',20);
add_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt',20);
add_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart',30);
add_action('woocommerce_single_product_summary', 'woocommerce_template_single_sharing',50);

/* Απενεργοποιούμε τα Reviews */

add_filter( 'wc_product_has_unique_sku', '__return_false', PHP_INT_MAX );
add_filter( 'woocommerce_product_tabs', 'sb_woo_remove_reviews_tab', 98);

function sb_woo_remove_reviews_tab($tabs) {

unset($tabs['reviews']);
return $tabs;

}
 
/* Μέχρι 4 προιόντα στα σχετικά προιόντα */
function woo_related_products_limit() {
    global $product;
    
    $args['posts_per_page'] = 4;
    return $args;
  }
  add_filter( 'woocommerce_output_related_products_args', 'jk_related_products_args' );
    function jk_related_products_args( $args ) {

    $args['posts_per_page'] = 4; // 4 related products
    $args['columns'] = 4; // arranged in 2 columns
    return $args;
}

/* Αφαίρεση των tabs στην αναλυτική */

remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );

/* Checkout */
/* Προσθήκη των ελληνικών νομών */

add_filter( 'woocommerce_states', 'custom_woocommerce_states' );

function custom_woocommerce_states( $states ) {

  $states['GR'] = array(
    'GR1' => 'Άγιον όρος',
    'GR2' => 'Αιτωλοακαρνανία',
    'GR3' => 'Αργολίδα',
    'GR4' => 'Αρκαδία',
    'GR5' => 'Άρτα',
    'GR6' => 'Αττική',
    'GR7' => 'Αχαΐα',
    'GR8' => 'Βοιωτία',
    'GR9' => 'Βόρειες σποράδες',
    'GR10' => 'Γρεβενά',
    'GR11' => 'Δράμα',
    'GR12' => 'Δωδεκάνησα',
    'GR13' => 'Έβρος',
    'GR14' => 'Εύβοια',
    'GR15' => 'Ευρυτανία',
    'GR16' => 'Ζάκυνθος',
    'GR17' => 'Ηλεία',
    'GR18' => 'Ημαθία',
    'GR19' => 'Ηράκλειο',
    'GR20' => 'Θάσος',
    'GR21' => 'Θεσπρωτία',
    'GR22' => 'Θεσσαλονίκη',
    'GR23' => 'Ιωάννινα',
    'GR24' => 'Καβάλα',
    'GR25' => 'Καρδίτσα',
    'GR26' => 'Καστοριά',
    'GR27' => 'Κέρκυρα',
    'GR28' => 'Κεφαλληνία',
    'GR29' => 'Κιλκίς',
    'GR30' => 'Κοζάνη',
    'GR31' => 'Κορινθία',
    'GR32' => 'Κυκλάδες',
    'GR33' => 'Λακωνία',
    'GR34' => 'Λάρισα',
    'GR35' => 'Λασίθι',
    'GR36' => 'Λέσβος',
    'GR37' => 'Λευκάδα',
    'GR38' => 'Μαγνησία',
    'GR39' => 'Μεσσηνία',
    'GR40' => 'Ξάνθη',
    'GR41' => 'Πέλλα',
    'GR42' => 'Πιερία',
    'GR43' => 'Πρέβεζα',
    'GR44' => 'Ρέθυμνο',
    'GR45' => 'Ροδόπη',
    'GR46' => 'Σαμοθράκη',
    'GR47' => 'Σάμος',
    'GR48' => 'Σέρρες',
    'GR49' => 'Τρίκαλα',
    'GR50' => 'Φθιώτιδα',
    'GR51' => 'Φλώρινα',
    'GR52' => 'Φωκίδα',
    'GR53' => 'Χαλκιδική',
    'GR54' => 'Χανιά',
    'GR55' => 'Χίος'
  );

  return $states;
}

// Προσθήκη validation του τηλεφώνου στο checkout 
add_action('woocommerce_checkout_process', 'my_custom_checkout_field_process');

function my_custom_checkout_field_process() {

  global $woocommerce;

      if ( ! (preg_match('/^[0-9]{10}$/D', $_POST['billing_phone'] ))){

          wc_add_notice( "To Τηλέφωνο πρέπει να αποτελείται από 10 ψηφία."  ,'error' );
      }
}

// Βγάζουμε το πεδίο billing διεύθυνση 2

add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );

function custom_override_checkout_fields( $fields ) {

unset($fields['billing']['billing_address_2']);

	return $fields;

}

// Απενεργοποιούμε την δυνατότητα αντικαταβολής πάνω από 500 ευρώ (βάση νόμου)
add_filter( 'woocommerce_available_payment_gateways', 'es_filter_gateways', 1);

function es_filter_gateways( $gateways ){
   global $woocommerce;

   if($woocommerce->cart->total > 500) {
   unset($gateways['cod']);
   }
   return $gateways;

}

// Προσθήκη της class woocommerce στην σελίδα προσφορών

add_filter( 'body_class','woocommerce_body_class' );

function woocommerce_body_class( $classes ) {
 
    if ( is_page_template( 'archive-product_sale.php' ) ) {
        $classes[] = 'woocommerce';
    }
     
    return $classes;
     
}

?>