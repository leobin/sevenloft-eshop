 <?php wp_list_comments( $args ); ?>

<?php comments_template( $file, $separate_comments ); ?>

<?php if ( is_singular() ) wp_enqueue_script( "comment-reply" ); ?>

<?php comment_form(); ?>

<?php paginate_comments_links(); ?>
