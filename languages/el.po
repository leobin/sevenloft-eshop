msgid ""
msgstr ""
"Project-Id-Version: Sevenloft Eshop\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-10-05 07:33+0000\n"
"PO-Revision-Date: 2020-10-05 07:33+0000\n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: el\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco https://localise.biz/\n"
"X-Loco-Version: 2.4.3; wp-5.5.1"

#: footer.php:64
msgid ""
"<a href=\"https://www.sevenloft.gr\" title=\"Κατασκευη Ιστοσελιδων "
"Θεσσαλονικη\" target=\"_blank\">Kατασκευή Ιστοσελίδας: SEVENLOFT</a>"
msgstr ""

#: contact.php:27
msgid ""
"<strong>ΤΗΛΕΦΩΝΙΚΟ ΚΕΝΤΡΟ</strong>:<br/>\n"
"                    Τηλέφωνο: 2310305312 <br/>\n"
"                    fax. 2310 324255<br/><br/>\n"
"                    <strong>ΕΔΡΑ ΚΑΤΑΣΤΗΜΑΤΟΣ</strong></br>\n"
"                    Διεύθυνση: Γ. Αγγέλου & Νεάρχου 1 <br/>\n"
"                    ΤΚ: 54250<br/>\n"
"                    Θεσσαλονίκη <br/><br/>\n"
"\n"
"                  <strong>ΩΡΑΡΙΟ ΕΞΥΠΗΡΕΤΗΣΗΣ ΠΕΛΑΤΩΝ</strong>:<br/>\n"
"                  Δευτέρα εώς Παρασκευή - 08:00 με 15:00 και 17.30 με 21."
"00<br/>\n"
"                  Σάββατο - 08:00 με 15:00<br/><br/>\n"
"                  <a class=\"color-black\" href=\"mailto:info@toolman.gr\">"
"info@toolman.gr</a>"
msgstr ""

#: justhome.php:193
msgid "[featured_products per_page=\"8\"]"
msgstr ""

#: justhome.php:218
msgid ""
"[products limit=\"8\" orderby=\"id\" order=\"DESC\" visibility=\"visible\"]\n"
msgstr ""

#: inc/core/class-wp-bootstrap-navwalker.php:365
msgid "Add a menu"
msgstr ""

#: inc/widgets/class-wc-widget-products.php:46
msgid "All Products"
msgstr ""

#: footer.php:63
msgid "All rights reserved"
msgstr ""

#: inc/widgets/class-wc-widget-products.php:67
msgid "ASC"
msgstr ""

#: inc/core/admin-functions.php:152
msgid "Checkbox Example"
msgstr ""

#: inc/core/admin-functions.php:155
msgid "Checkbox example description."
msgstr ""

#. Name of the template
msgid "Contact"
msgstr ""

#: inc/widgets/class-wc-widget-products.php:56
msgid "Date"
msgstr ""

#: inc/widgets/class-wc-widget-products.php:68
msgid "DESC"
msgstr ""

#: inc/widgets/class-wc-widget-products.php:24
msgid "Display a list of your products on your site."
msgstr ""

#: inc/widgets/class-wc-widget-products.php:47
msgid "Featured Products"
msgstr ""

#: footer.php:43
msgid "FIND US"
msgstr ""

#: inc/core/wordpress-functions.php:320
msgid "Footer Menu 1"
msgstr ""

#: inc/core/wordpress-functions.php:321
msgid "Footer Menu 2"
msgstr ""

#: inc/core/wordpress-functions.php:322
msgid "Footer Menu 3"
msgstr ""

#: inc/core/wordpress-functions.php:323
msgid "Footer Menu 4"
msgstr ""

#: page_left.php:27 single.php:172 page.php:43 contact.php:52
msgid "H σελίδα δεν βρέθηκε"
msgstr ""

#: inc/widgets/class-wc-widget-products.php:74
msgid "Hide free products"
msgstr ""

#. Name of the template
msgid "Home"
msgstr ""

#. Author URI of the theme
msgid "https://sevenloft.gr"
msgstr ""

#. URI of the theme
msgid "https://www.sevenloft.gr/service/kataskevi-istoselidon/"
msgstr ""

#: inc/core/admin-functions.php:161
msgid "Input Example"
msgstr ""

#: inc/widgets/class-wc-widget-products.php:39
msgid "Number of products to show"
msgstr ""

#: inc/widgets/class-wc-widget-products.php:48
msgid "On-sale Products"
msgstr ""

#: inc/core/admin-functions.php:176
msgid "Option 1"
msgstr ""

#: inc/core/admin-functions.php:177
msgid "Option 2"
msgstr ""

#: inc/core/admin-functions.php:178
msgid "Option 3"
msgstr ""

#: inc/widgets/class-wc-widget-products.php:54
msgid "Order by"
msgstr ""

#: inc/widgets/class-wc-widget-products.php:57
msgid "Price"
msgstr ""

#: inc/core/wordpress-functions.php:319
msgid "Primary Menu"
msgstr ""

#: inc/widgets/class-wc-widget-products.php:30
msgid "Products"
msgstr ""

#: inc/widgets/class-wc-widget-products.php:58
msgid "Random"
msgstr ""

#. Name of the template
msgid "Sale Page"
msgstr ""

#: inc/widgets/class-wc-widget-products.php:59
msgid "Sales"
msgstr ""

#: inc/core/admin-functions.php:170
msgid "Select Example"
msgstr ""

#. Name of the theme
msgid "Sevenloft Eshop"
msgstr ""

#. Author of the theme
msgid "Sevenloft IKE"
msgstr ""

#. Description of the theme
msgid ""
"Sevenloft is our starter theme for our woocommerce projects. Designed and "
"developed by Sevenloft Core developers"
msgstr ""

#: inc/core/admin-functions.php:77 inc/core/admin-functions.php:78
msgid "Sevenloft Theme"
msgstr ""

#: inc/widgets/class-wc-widget-products.php:44
msgid "Show"
msgstr ""

#: inc/widgets/class-wc-widget-products.php:79
msgid "Show hidden products"
msgstr ""

#. Name of the template
msgid "Simple Page"
msgstr ""

#: inc/widgets/class-wc-widget-products.php:65
msgctxt "Sorting order"
msgid "Order"
msgstr ""

#: inc/core/admin-functions.php:142
msgid "Theme Options"
msgstr ""

#: inc/widgets/class-wc-widget-products.php:31
msgid "Title"
msgstr ""

#: inc/widgets/class-wc-widget-products.php:26
msgid "WooCommerce Products"
msgstr ""

#: archive.php:34
msgid "Διαβάστε περισσότερα..."
msgstr ""

#: footer.php:27
msgid "ΕΤΑΙΡΙΑ"
msgstr ""

#: contact.php:18
msgid "Επικοινωνήστε μαζί μας συμπληρώνοντας την φόρμα επικοινωνίας."
msgstr ""

#: sidebar-shop_left.php:2
msgid "Επιλογή Φίλτρων"
msgstr ""

#: 404.php:6
msgid "Η σελίδα δε βρέθηκε!"
msgstr ""

#: archive.php:52
msgid ""
"Η σελίδα που αναζητήσατε μπορεί να έχει διαγραφεί, να έχει μετονομαστεί ή να "
"μην είναι διαθέσιμη. Παρακαλούμε, πατήστε <a href=\"/\">εδώ</a> έτσι ώστε να "
"οδηγηθείτε στην αρχική σελίδα."
msgstr ""

#: archive.php:49
msgid "Η σελίδα που ζητήσατε δεν βρέθηκε."
msgstr ""

#: contact.php:19
msgid ""
"Κάποιος εκπρόσωπος μας θα αναλάβει να σας απαντήσει άμεσα γι’ αυτό είναι "
"απαραίτητη η σωστή εισαγωγή των στοιχείων σας."
msgstr ""

#: 404.php:8
msgid "Μπορείτε να επιστρέψετε στην.... <a href=\"/\">Αρχική Σελίδα »</a>"
msgstr ""

#: archive-product_sale.php:11
msgid "Προσφορές"
msgstr ""

#: 404.php:7
msgid ""
"Σας ζητάμε συγνώμη για την αναστάτωση. Η σελίδα που προσπαθήσατε να δείτε "
"δεν υπάρχει. Πιθανόν να βρεθήκατε εδώ από ένα λάθος link."
msgstr ""

#: single.php:113
msgid "Σχετικά προϊόντα"
msgstr ""

#: footer.php:35
msgid "ΥΠΟΣΤΗΡΙΞΗ"
msgstr ""
