<?php get_header(); ?>



<?php if(have_posts()) : ?>

  <?php while(have_posts()) : the_post(); ?>

    <div class="container">

      <div class="row">

        <div class="col-md-12 col-sm-12 col-12">

          <div class="page-bg">

             <h1 class="page-title">

                <?php the_title(); ?>

              </h1>

              <div class="simple-page-content"><?php the_content(); ?></div>

          </div>

        </div>

      </div>

    </div>

  <?php endwhile; ?>



<?php else : ?>



<h2>

  <?php _e( 'H σελίδα δεν βρέθηκε', 'sevenloft-eshop' ); ?>

</h2>



<?php endif; ?>

<?php get_footer(); ?>