<?php get_header(); ?>
<section class="404page">
	<div class="container">
		<div class="row">
			<div class="col-12">
			    <h1><?php _e( 'Η σελίδα δε βρέθηκε!', 'sevenloft-eshop' ); ?></h1>
			    <p><?php _e( 'Σας ζητάμε συγνώμη για την αναστάτωση. Η σελίδα που προσπαθήσατε να δείτε δεν υπάρχει. Πιθανόν να βρεθήκατε εδώ από ένα λάθος link.', 'sevenloft-eshop' ); ?></p>
				<p><?php _e( 'Μπορείτε να επιστρέψετε στην.... <a href="/">Αρχική Σελίδα »</a>', 'sevenloft-eshop' ); ?></p>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>