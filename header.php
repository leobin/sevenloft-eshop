<!DOCTYPE html>

<html <?php language_attributes(); ?>>
<head>
	
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

	<?php 
		global $wpdb;
		global $woocommerce;
		global $order;
		global $current_user;
	?>
    
	<?php wp_head(); ?>

</head>

<?php echo '<body class="'.join(' ',get_body_class()).'">'.PHP_EOL; ?>

<div class="overlay"></div>
<div class="overlayBasket"></div>


<!-- header of mywebdesign -->
<div class="row-offcanvas row-offcanvas-left section-bg">	
	<header>
		<div class="container-fluid topheader">
			<div class="container">
				<div class="top-bar">
					<!-- <?php do_action('sevenloft_header_topheader'); ?> -->
					<span class="info-til">Τηλέφωνο παραγγελιών: <a href="tel:+302310555445">+30 2310 555 445</a></span>
					<span class="info-til"><a href="#">Εγγραφή στο B2B</a></span>
				</div>
			</div>
		</div>
		<div class="header">
			<div class="container">
				<div class="row">
					<div class="col-md-3 col-sm-3 d-md-block d-sm-none d-xs-none">
						<div class="logo-wr">
							<?php if ( function_exists( 'the_custom_logo' ) ) {
							 the_custom_logo();
							} ?>
						</div>
					</div>
					<div class="col-md-7 col-sm-4 col-xs-7">
						<div class="search-wr">
							<?php dynamic_sidebar( 'Search_Bar' ); ?>
						</div>					
					</div>

					<div class="col-md-2 col-sm-8 col-xs-5 dflexheadi">
						<div class="header-cart-wr">
							<span class="header-cart-btn"><a class="header-stores-btn account-btn-xs" href="/cart/"><img class="img-fluid" src="/wp-content/themes/sevenloft-eshop/assets/images/addtocart.png"><span class="header-cart-count">(<?php echo $woocommerce->cart->cart_contents_count ?>)</span></a></span>
<span class="header-cart-btn"><a class="header-stores-btn account-btn-xs" href="/myaccount/"><i class="fa fa-user" aria-hidden="true"></i></a></span>
<span class="header-cart-btn"><a class="header-stores-btn store-btn-xs" href="/to-katastima-mas/"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span>
						</div>
					</div>
				</div>
			</div>
				<div id="fixedmenu" class="menu main-menu">
					<div class="container">
						<div class="row">
							<div class="col-6">
							 	<nav class="navbar navbar-expand-md">

								    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-controls="bs-example-navbar-collapse-1" aria-expanded="false" aria-label="Toggle navigation">
								        <span class="navbar-toggler-icon top"></span>
								        <span class="navbar-toggler-icon middle"></span>
								        <span class="navbar-toggler-icon bottom"></span>
								    </button>
								    
								        <?php
								        wp_nav_menu( array(
								            'theme_location'    => 'primary',
								            'depth'             => 5,
								            'container'         => 'div',
								            'container_class'   => 'collapse navbar-collapse',
								            'container_id'      => 'bs-example-navbar-collapse-1',
								            'menu_class'        => 'nav navbar-nav',
								            'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
								            'walker'            => new WP_Bootstrap_Navwalker(),
								        ) );
								        ?>
								   
								</nav>
					
							</div>
							<div class="col-6 logo-mobile">	
								<?php if ( function_exists( 'the_custom_logo' ) ) {
								 the_custom_logo();
								} ?>
							</div>
						</div>
					</div>
				</div>
			
		</div>
	</header>
