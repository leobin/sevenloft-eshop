<?php do_action('sevenloft_footer_before_footer'); ?>

<footer>

  <div class="container">
    <div class="row">
      <div class="col-12 p-md-0">
        <div class="mailchp">
           <?php echo do_shortcode('[mc4wp_form id="24130"]'); ?>
        </div>
       
      </div>
       
    </div>
    <div class="row">
      <div class="col-md-3 col-sm-6 col-xs-6">
        <div class="f-menu">
          <div class="f-menu-title">LM Company</div>
           <ul>
            <span>Η Εταιρεία LMcompany ιδρύθηκε το 2006 ως μια εταιρεία που δραστηριοποιείται στο ΧΟΝΔΡΙΚΟ ΕΜΠΟΡΙΟ(ειδή οικιακής χρήσεις-αξεσουάρ γυναίκεια-καλλυντικά-κτλ).</span>
          </ul>
          
        </div>
      </div>
      <div class="col-md-3 col-sm-6 col-xs-6">
        <div class="f-menu">
          <div class="f-menu-title"><?php _e( 'ΕΤΑΙΡΙΑ', 'sevenloft-eshop' ); ?></div>
           
             <?php wp_nav_menu( array( 'theme_location' => 'footer2' ) ); ?>
          
        </div>
      </div>
      <div class="col-md-3 col-sm-6 col-12">
        <div class="f-menu">
          <div class="f-menu-title"><?php _e( 'ΥΠΟΣΤΗΡΙΞΗ', 'sevenloft-eshop' ); ?></div>
          
             <?php wp_nav_menu( array( 'theme_location' => 'footer3' ) ); ?>
          
        </div>
      </div>
      <div class="col-md-3 col-sm-6 col-12 tx-right tx-sm-left">
        <div class="f-social">
          <div class="f-menu-title"><?php _e( 'FIND US', 'sevenloft-eshop' ); ?></div>
          <a class="fb-btn" target="_blank" href="https://www.facebook.com/#"><i aria-hidden="true"
              class="fa fa-facebook"></i></a>
          <a class="maps-btn" target="_blank" href="#"><i class="fa fa-map-marker" aria-hidden="true"></i></a>
          <a class="instagram-btn" target="_blank" href="https://www.instagram.com/#"><i class="fa fa-instagram"
              aria-hidden="true"></i></a>
        </div>
         <div class="cardsp">
           <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/images/cards.png" alt="<?php the_title(); ?>">
        </div>
      </div>
    </div>
   </div>
</footer>

<?php do_action('sevenloft_footer_after_footer'); ?>

<div class="footer-info">
  <div class="container">
    <div class="info_flex">
      <span class="info-text"> <?php get_bloginfo ( 'name' ) ; ?> © <?php echo date('Y'); ?> - <?php _e( 'All rights reserved', 'sevenloft-eshop' ); ?></span>
      <span class="info-text"><?php _e( '<a href="https://www.sevenloft.gr" title="Κατασκευη Ιστοσελιδων Θεσσαλονικη" target="_blank">Kατασκευή Ιστοσελίδας: SEVENLOFT</a>', 'sevenloft-eshop' ); ?></span>
    </div>

  </div>
</div>

</div> <!-- Offcanvas section end -->

	<button id="button" title="Go to top"><i class="fa fa-arrow-up" aria-hidden="true"></i></button>

<?php wp_footer(); ?>

</body>

</html>